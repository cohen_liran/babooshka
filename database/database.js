"use strict";

var settings = require('../scripts/settings');
var db = require('./dbconnection');
var utils = require('../scripts/utils');

const TABLE = 'stubs';
const ID = '_id';
const METHOD = 'method';
const ALIAS = 'alias';
const PATH = 'path';
const CHANNEL = 'channel';
const INDEXING = 'indexing';
const RESPONSE = 'response';
const CODE = 'code';
const ACTIVE = 'active';
const DELAY = 'delay';

const TABLE_CHANNELS = 'channels';

const SETTINGS_TABLE = 'settings';
const NAME = 'name';
const VALUE = 'value';

const NO_CHANNEL_ID = -1;

//initialize the database so we can start calling it
var initDb = function(){
    db.initialize('./'+utils.getEnv('DATABASE'), function(db){
        db.run("CREATE TABLE IF NOT EXISTS "+TABLE_CHANNELS+" ("+
            ID+" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            CHANNEL+" TEXT UNIQUE NOT NULL," +
            INDEXING+" INTEGER NOT NULL DEFAULT 0" +
            ")",function() {

            db.run("CREATE TABLE IF NOT EXISTS " + TABLE + " (" +
                METHOD + " TEXT NOT NULL DEFAULT 'GET'," +
                ALIAS + " TEXT," +
                PATH + " TEXT NOT NULL," +
                CHANNEL+" INTEGER NOT NULL DEFAULT "+NO_CHANNEL_ID+"," +
                RESPONSE + " TEXT," +
                CODE + " INTEGER NOT NULL DEFAULT 200," +
                ACTIVE + " INTEGER NOT NULL DEFAULT 1 CHECK(" + ACTIVE + " in(0,1))," +
                DELAY + " INTEGER NOT NULL DEFAULT 0," +
                "PRIMARY KEY (" + PATH + "," + METHOD + "," + CHANNEL + ")" +
                ")",
                function () {

                    db.run("CREATE TABLE IF NOT EXISTS " + SETTINGS_TABLE + " (" +
                        NAME + " TEXT NOT NULL PRIMARY KEY," +
                        VALUE + " TEXT" +
                        ")",
                        function () {
                            let operation = "INSERT OR IGNORE INTO " + SETTINGS_TABLE + " (" + NAME + "," + VALUE + ") VALUES (?,?)";
                            //populate the required settings if not already set
                            db.run(operation, [
                                "forwardUrl", process.env.FORWARD_HOST||"https://google.com"
                            ]);
                            db.run(operation, [
                                "forwardProtocol", "HTTP"
                            ]);
                            db.run(operation, [
                                "validateJSONResponse", true
                            ]);
                            db.run(operation, [
                                "allowSelfSignedCert", false
                            ]);
                            db.run(operation, [
                                "allowChannelFallThrough", false
                            ]);

                            updateDatabase(db);
                        });
                });
        });
    });
};

initDb();

//Channels
module.exports.getAllChannels = function(){
    return db.all("SELECT * FROM "+TABLE_CHANNELS);
};

module.exports.getChannelByName = function(name){
    return db.one("SELECT * FROM "+TABLE_CHANNELS+" WHERE "+CHANNEL+"=?", name ? name.toLowerCase() : null);
};

module.exports.getChannelById = function(id){
    return db.one("SELECT * FROM "+TABLE_CHANNELS+" WHERE "+ID+"=?", id);
};

module.exports.insertChannel = function(channelName){
    verifyChannel(channelName);
    return db.one("INSERT INTO "+TABLE_CHANNELS+"("+
        CHANNEL +
        ") VALUES(?)", channelName.toLowerCase());
};

module.exports.updateChannel = function(channel){
    verifyChannel(channel.channel);
    return db.one("UPDATE "+TABLE_CHANNELS+" SET "+
        CHANNEL+"=?"+
        " WHERE "+ID+"=?"
        , [channel.channel.toLowerCase(), channel._id]);
};

module.exports.updateChannelIndexingState = function(channelId, index){
    return db.one("UPDATE "+TABLE_CHANNELS+" SET "+
        INDEXING+"=?"+
        " WHERE "+ID+"=?"
        , [index ? 1 : 0, channelId])
        .then(function(){
            return module.exports.getChannelById(channelId);
    });
};

module.exports.deleteChannel = function(channelId){
    return db.run("DELETE from "+TABLE_CHANNELS+" WHERE "+ID+"=?", channelId)
        .then(function(){
            return db.run("DELETE from "+TABLE+" WHERE "+CHANNEL+"=?", channelId);
        });
};

//Stubs
module.exports.getAllStubs = function(){
    return db.all("SELECT rowid as "+ID+",* FROM "+TABLE+" ORDER BY rowid DESC");
};

module.exports.getStubById = function(id){
    return db.one("SELECT rowid as "+ID+",* FROM "+TABLE+" WHERE rowid=?", id);
};

module.exports.getActiveStubByUrlMethodAndChannel = function(path,method,channel){
    return db.one("SELECT rowid as "+ID+",* FROM " + TABLE + " WHERE " + PATH + "=? AND " + METHOD + "=? AND " + ACTIVE + "=1 AND " + CHANNEL + "=?", [getFixedPath(path), getFixedMethod(method), channel || NO_CHANNEL_ID]);
};

module.exports.getActiveStubsWithWildCard = function(method,channel){
    return db.all("SELECT rowid as "+ID+",* FROM "+TABLE+" WHERE "+PATH+" LIKE '%<\\?>%' ESCAPE '\\' AND "+METHOD+"=? AND "+ACTIVE+"=1 AND " + CHANNEL + "=?", [getFixedMethod(method),channel || NO_CHANNEL_ID]);
};

module.exports.deleteStubWithId = function(id){
    return db.run("DELETE from "+TABLE+" WHERE rowid=?", id);
};

module.exports.insert = function(stub){
    return db.one("INSERT INTO "+TABLE+"("+
        METHOD+","+
        ALIAS+","+
        PATH+","+
        RESPONSE+","+
        CODE+","+
        ACTIVE+","+
        CHANNEL+","+
        DELAY+
        ") VALUES(?,?,?,?,?,?,?,?)", [getFixedMethod(stub.method), getFixedAlias(stub.alias),getFixedPath(stub.path), getFixedResponse(stub.response), stub.code, stub.active, stub.channel, stub.delay || 0]);
};

module.exports.updateStub = function(stub){
    return db.one("UPDATE "+TABLE+" SET "+
        METHOD+"=?,"+
        ALIAS+"=?,"+
        PATH+"=?,"+
        RESPONSE+"=?,"+
        CODE+"=?,"+
        CHANNEL+"=?,"+
        DELAY+"=?"+
        " WHERE rowid=?"
        , [getFixedMethod(stub.method), getFixedAlias(stub.alias),getFixedPath(stub.path), getFixedResponse(stub.response), stub.code, stub.channel, stub.delay || 0, stub._id]);
};

module.exports.insertOrReplace = function(stub) {
    return db.one("INSERT OR REPLACE INTO "+TABLE+"("+
        METHOD+","+
        ALIAS+","+
        PATH+","+
        RESPONSE+","+
        CODE+","+
        ACTIVE+","+
        CHANNEL+","+
        DELAY+
        ") VALUES(?,?,?,?,?,?,?,?)", [getFixedMethod(stub.method), getFixedAlias(stub.alias),getFixedPath(stub.path), getFixedResponse(stub.response), stub.code, stub.active, stub.channel, stub.delay || 0]);
};

module.exports.toggleStubActiveState = function(stub){
    return db.one("UPDATE "+TABLE+" SET "+
        ACTIVE+"=?"+
        " WHERE rowid=?"
        , [stub.active, stub._id]);
};


//settings
module.exports.getSettings = function(){
    return db.all("SELECT * FROM "+SETTINGS_TABLE);
};

module.exports.updateSettings = function(updates){
    let operation = "INSERT OR REPLACE INTO "+SETTINGS_TABLE+" ("+NAME+","+VALUE+") VALUES (?,?)";

    var updateOperations = [
        db.run('BEGIN TRANSACTION'),
        db.all(operation, ['forwardUrl', getFixedForwardUrl(updates.forwardUrl)]),
        db.all(operation, ['forwardProtocol', getFixedProtocol(updates.forwardProtocol)]),
        db.all(operation, ['validateJSONResponse', Boolean(updates.validateJSONResponse)]),
        db.all(operation, ['allowSelfSignedCert', Boolean(updates.allowSelfSignedCert)]),
        db.all(operation, ['allowChannelFallThrough', Boolean(updates.allowChannelFallThrough)]),
    ];

    return Promise.all(updateOperations)
        .then(function(){
            return module.exports.getSettings();
        }).then(newSettings=>{
            settings.loadSettings(newSettings);
        }).then(function(){
            return db.run('END TRANSACTION');
        }).catch(err=>{
            db.run('ROLLBACK');
            throw err;
        });
};

function getStubByUrlMethodAndChannel(path,method,channel) {
    return db.one("SELECT * FROM " + TABLE + " WHERE " + PATH + "=? AND " + METHOD + "=? AND " + CHANNEL + "=?", [getFixedPath(path), getFixedMethod(method), channel || NO_CHANNEL_ID]);
}

function getFixedAlias(alias){
    return alias ? alias.trim() : alias;
}

function getFixedPath(path){
    var formattedPath = path;

    //remove leading slashes
    while(formattedPath.startsWith('/')){
        formattedPath = formattedPath.slice(1);
    }

    //detect and remove query params
    let paramStartIndex = 0;
    while((paramStartIndex = formattedPath.indexOf('?',paramStartIndex)) >=0 && paramStartIndex < formattedPath.length){
        if(paramStartIndex === 0 ||
            paramStartIndex === formattedPath.length -1 ||
            formattedPath.slice(paramStartIndex-1,paramStartIndex+2) != "<?>"){
            formattedPath = paramStartIndex > 0 ? formattedPath.slice(0, paramStartIndex) : "";
            break;
        } else {
            paramStartIndex++;
        }
    }

    //remove tailing slashes
    while(formattedPath.endsWith('/')){
        formattedPath = formattedPath.slice(0,-1);
    }

    formattedPath = formattedPath.toLowerCase();
    
    //check for illegal path
    if(formattedPath.length === 0){
        throw new Error("Invalid path, path must contain parameter-less uri part");
    }
    if(formattedPath.indexOf(" ") != -1){
        throw new Error("Invalid path, path may not contain spaces");
    }
    if(formattedPath === '<?>'){
        throw new Error("Illegal path, path cannot be only wild card");
    }
    if(formattedPath.startsWith('babooshka')){
        throw new Error("Illegal path, path may not start with 'babooshka'");
    }
    
    return formattedPath;
}

function getFixedMethod(method){
    return method ? method.toUpperCase() : 'GET';
}

function getFixedForwardUrl(url){
    var formattedUrl = url;

    //remove spaces
    formattedUrl = formattedUrl.replace(' ','');

    //remove protocol
    if(formattedUrl.startsWith('http://') || formattedUrl.startsWith('https://')){
        formattedUrl = formattedUrl.slice(formattedUrl.indexOf('://')+3);
    }

    //detect and remove query params
    var paramStart = formattedUrl.indexOf('?');
    if(paramStart > -1){
        formattedUrl = formattedUrl.slice(0,paramStart);
    }

    //remove tailing slashes
    while(formattedUrl.endsWith('/')){
        formattedUrl = formattedUrl.slice(0,-1);
    }

    return formattedUrl;
}

function getFixedResponse(response){
    return response || '{}';
}

function getFixedProtocol(protocol){
    return (protocol|| 'HTTP').toUpperCase();
}

function updateDatabase(db){
    console.log("Database is up to date");
}

let verifyChannel = function(channel){
    if(!channel || new RegExp(/[^A-z\d-]/gi).test(channel)){
        throw new Error('Invalid channel name')
    }
};

module.exports.getFixedProtocol = getFixedProtocol;
module.exports.getFixedPath = getFixedPath;

