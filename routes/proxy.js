"use strict";

var express = require('express');
var request = require('request');
var router = express.Router();
let db = require('../database/database');
let utils = require('../scripts/utils');
let zlib = require('zlib');

const ALLOWED_CONTENT_TYPE_INDEXING = [
    'application/json',
    'application/xml',
    'text/plain',
    'text/xml'
];

const ANY_ROUTE = '/*';

/** check if the incoming request is mapped by the app and either
 * return stub data or forward the request to server
 * @param req the request object to be processed
 * @param res the response object to be used to notify caller
 * @param next the next request handler
 */
var handleRequest = function(req,res,next){
    var channelAndPath = breakChannelFromPath(req._parsedUrl.pathname);
    db.getChannelByName(channelAndPath.channel)
        .then(channel=>{
            if(channel){
                //detected channel
                req.originalUrl = req.originalUrl.replace(req._parsedUrl.pathname, channelAndPath.path);
                req._parsedUrl.pathname = channelAndPath.path;
            }

            return db.getActiveStubByUrlMethodAndChannel(req._parsedUrl.pathname, req.method, channel ? channel._id : undefined)
                .then((stub) => {
                    if (stub) {
                        return stub;
                    } else if (envToBool(utils.getEnv('ALLOW_CHANNEL_FALL_THROUGH')) && channel) {
                        return db.getActiveStubByUrlMethodAndChannel(req._parsedUrl.pathname, req.method, undefined)
                    }
                })
                .then(stub => {
                    if (stub) {
                        //path is mapped to an active stub
                        return stub;
                    } else {
                        //path is not mapped to an explicit active stub, check if it mapped using wild cards
                        return db.getActiveStubsWithWildCard(req.method, channel ? channel._id : undefined)
                            .then(stubs => {
                                if (stubs) {
                                    return stubs;
                                } else if (envToBool(utils.getEnv('ALLOW_CHANNEL_FALL_THROUGH')) && channel) {
                                    return db.getActiveStubsWithWildCard(req.method, undefined);
                                }
                            });
                    }
                }).then(stubs => {
                    if (stubs && !Array.isArray(stubs)) {
                        return stubs;
                    } else if (stubs) {
                        for (let stub of stubs) {
                            let parts = stub.path.split('<?>');
                            let regexString;

                            //construct the following regex statement: ^/something/like/[^/]+/this$
                            for (let i = 0; i < parts.length; i++) {
                                if (i === 0) regexString = "/";
                                regexString += parts[i];
                                if (i < parts.length - 1) {
                                    regexString += "[^/]+"
                                }
                            }
                            if (regexString) {
                                let regex = new RegExp("^" + regexString + "$", 'i');
                                if (regex.test(req._parsedUrl.pathname)) {
                                    return stub;
                                }
                            }
                        }
                    }
                }).then(stub => {
                    //if channel is under live indexing ignore it
                    return channel && channel.indexing ? null : stub;
                }).then(stub => {
                    if (stub) {
                        if (stub.delay > 0) {
                            setTimeout(function () {
                                res.status(stub.code);
                                res.send(stub.response);
                            }, stub.delay);
                        } else {
                            res.status(stub.code);
                            res.send(stub.response);
                        }
                    } else {
                        //either has no matching path, stub is inactive or channel is being live-indexed, forward the request to the server
                        forwardMessage(req, res, next, channel && channel.indexing ? channel : undefined);
                    }
                })
                .catch(err => {
                    //something went wrong
                    next(err);
                });
        });

};

var envToBool = function(env){
    //because env vars are always strings for some reason
    if(env === true || env === false) return env;
    else return env ==='true';
};

var breakChannelFromPath = function(path){
    var pathParts = new RegExp(/\/?baboocha-(.+?)(\/.*)/gi).exec(path);
    if(pathParts && pathParts.length > 2){
        return {path: pathParts[2], channel: pathParts[1]};
    }
    return {path: path};
};

/** forward the message to the remote server and return the results to the caller through
 * the response object */
var forwardMessage = function(req, res, next, indexedChannel) {
    let proxyRequest = {
        method: req.method,
        url: getUrlWithQueryParam(req),
        headers: getHeaders(req),
        body: getBody(req),
        rejectUnauthorized: getRejectUnauthorized(req),
    };
    if(process.env.DEBUG){
       console.log('forwarding:',proxyRequest);
    }

    var proxy = request(proxyRequest);

    if(indexedChannel) {

        var responseEncoding = "";
        var incomingData = [];
        var stub = {
            method: proxyRequest.method,
            alias: proxyRequest.method+" "+req.originalUrl,
            path: req.originalUrl,
            response: "{}",
            code: 200,
            channel: indexedChannel._id,
            active: 1,
        };

        // collect incoming data
        proxy.on('data', function(chunk){
            if(stub) incomingData.push(chunk);
        });

        // sample response parameters
        proxy.on('response', function (response) {
            try {
                //ensure only json/plain text requests are indexed to avoid keeping files in stubs
                var contentType = response.headers['content-type'] || "";
                var dataRequest = false;
                for(let allowCT of ALLOWED_CONTENT_TYPE_INDEXING){
                    if(contentType.indexOf(allowCT) >= 0){
                        dataRequest = true;
                        break;
                    }
                }
                if(!dataRequest){
                    stub = null;
                    return;
                }

                responseEncoding = response.headers['content-encoding'];
                if(response.statusCode >= 400) {
                    stub = null;
                }
            } catch (e) {
                console.log('failed to index route', e);
                stub = null;
            }
        });

        //when traffic complete add stub database
        proxy.on('end', function () {
            if(stub) {
                try {
                    if (incomingData.length > 0) {
                        var bodyBuffer = Buffer.concat(incomingData);
                        if(responseEncoding.indexOf("gzip") > -1){
                            //gzip body type, need to unzip before indexing
                            zlib.unzip(bodyBuffer,
                                { finishFlush: zlib.constants.Z_SYNC_FLUSH },
                                (err, buffer)=>{
                                    if(err){
                                        console.log("failed to unzip zipped body: ",err);
                                        stub = null;
                                        return;
                                    }
                                    stub.response = buffer.toString() || "{}";
                                    db.insertOrReplace(stub)
                                        .then(()=>{
                                            console.log('Indexed endpoint:',stub.alias);
                                        })
                                        .catch(error => {
                                            console.log('failed to insert new stub into database',error);
                                        });
                            });
                        } else {
                            //normal body type
                            stub.response = bodyBuffer.toString() || "{}";
                            db.insertOrReplace(stub)
                                .then(()=>{
                                    console.log('Indexed endpoint:',stub.alias);
                                })
                                .catch(error => {
                                    console.log('failed to insert new stub into database',error);
                                });
                        }
                    }
                } catch (e) {
                    console.log('failed to index route response body', e);
                }
            }
        });
    }

    proxy.on('error', function(error) {
        var err = new Error("Failed to forward request to server");
        err.stacktrace = error;
        next(err);
    });

    proxy.pipe(res);
};

/** return a url pointing to the external server with the same path as
 * the one requested, the resulting path already include all query params
 */
var getUrlWithQueryParam = function (req){
    return getProtocol(req)+'://'+(getCleanUrl(req.headers.babooshka_url) || (utils.getEnv('FORWARD_HOST') + utils.getEnv('FORWARD_BASE_URL',''))) + req.originalUrl;
};

/** return a well formed header block for the outgoing request. */
var getHeaders = function(req){
    let overrideHost = req.headers.babooshka_url ? getCleanUrl(req.headers.babooshka_url).split('/')[0] : undefined;

    //cleanup the headers from any littering babooshka overrides
    delete req.headers.babooshka_url;
    delete req.headers.babooshka_protocol;

    //create a copy of the headers to avoid messing up the request headers
    var headers = JSON.parse(JSON.stringify(req.headers));
    headers.host = overrideHost || process.env.FORWARD_HOST;

    return headers;
};

/** return the body content of the request as a string or undefined if body is empty */
var getBody = function(req){
    return req.hasBody ? req.body : undefined;
};

/** return whether or not the request should allow untrusted/self signed certificate */
var getRejectUnauthorized = function(req){
    return !envToBool(utils.getEnv('ALLOW_UNTRUSTED_CRT'));
};

var getProtocol = function(req){
    return req.headers.babooshka_protocol||utils.getEnv('FORWARD_PROTOCOL');
};

var getCleanUrl = function(url){
    if(url) {
        var clean = url.toLowerCase();

        clean = (clean+'').replace(/\s/g, '');

        if(clean.indexOf("://") > -1 && clean.indexOf("://") < 8){
            clean = clean.slice(clean.indexOf("://")+3);
        }

        while (clean.startsWith('/')) {
            clean = clean.slice(1);
        }
        while (clean.endsWith('/')) {
            clean = clean.slice(-1);
        }

        //check for illegal path
        if(clean.length === 0 || clean.indexOf('?') > -1){
            throw new Error("Invalid path, path must contain parameter-less uri part");
        }
        if(clean.indexOf(" ") != -1){
            throw new Error("Invalid path, path may not contain spaces");
        }
        if(clean.startsWith('babooshka')){
            throw new Error("Illegal path, path may not start with 'babooshka'");
        }

        return clean;
    }
};

/* Catch all api endpoints not mapped by actual server and forward the request */
router.get(ANY_ROUTE, handleRequest);
router.post(ANY_ROUTE, handleRequest);
router.put(ANY_ROUTE, handleRequest);
router.delete(ANY_ROUTE, handleRequest);
router.patch(ANY_ROUTE, handleRequest);

module.exports = router;
