"use strict";

var express = require('express');
var router = express.Router();
let db = require('../database/database');
let settings = require('../scripts/settings');
let utils = require('../scripts/utils');

router.get('/',function(req,res,next){
    db.getSettings()
        .then(settingsArr=>{
            let fullSettings = settings.settingsAsObject(settingsArr);
            fullSettings.version = utils.getEnv('APP_VERSION');
            res.send(fullSettings);
        })
        .catch(err=>{
            next(err);
        });
});

router.put('/',function(req,res,next){
    if(req.hasBody && req.body.forwardUrl && req.body.forwardProtocol){
        db.updateSettings(req.body)
            .then(function(){
                res.send();
            })
            .catch(err=>{
                next(err);
            });
    } else {
        var errorMessage = 'An unknown error';
        if(!req.body.forwardUrl){
            errorMessage = 'Url cannot be blank'
        } else if(!req.body.forwardProtocol){
            errorMessage = 'Missing default protocol'
        }
        var err = new Error(errorMessage);
        err.status = 400;
        next(err);
    }
});

module.exports = router;
