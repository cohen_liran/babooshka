"use strict";

var express = require('express');
var router = express.Router();
let db = require('../database/database');

router.get('/',function(req,res,next){
    if(req.hasQuery && req.query.id){
        db.getStubById(req.query.id)
            .then(stub=>{
                res.send(stub);
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        db.getAllStubs()
            .then(stubs=>{
                res.send(stubs);
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    }
});

router.delete('/',function(req,res,next){
    if(req.hasQuery && req.query.id){
        db.deleteStubWithId(req.query.id)
            .then(deletedRows=>{
                res.send({});
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var err = new Error('missing query param "id"');
        err.status = 400;
        next(err);
    }
});

router.put('/',function(req,res,next){
    if(req.hasBody && req.body.path && req.body.code && req.body.method && (req.body.delay !== undefined)){
        db.updateStub(req.body)
            .then(updatedStub=>{
                res.send({});
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var missingErr;
        if(!req.body.code){
            missingErr = 'code';
        } else if (!req.body.path){
            missingErr = 'path';
        } else if (!req.body.method) {
            missingErr = 'method';
        } else if (req.body.delay !== undefined){
            missingErr = 'delay';
        } else {
            missingErr = 'body';
        }
        var err = new Error('missing '+missingErr);
        err.status = 400;
        next(err);
    }
});

router.put('/toggle',function(req,res,next){
     if(req.hasBody && req.body.active !== undefined){
         db.toggleStubActiveState(req.body)
             .then(updatedStub=>{
                res.send({});
             })
             .catch(err=>{
                 next(parseSQLError(err));
             });
     } else {
         var err = new Error('missing body part');
         err.status = 400;
         next(err);
    }
});

router.post('/',function(req,res,next){
    if(req.hasBody && req.body.path){
        db.insert(req.body)
            .then(newStub=>{
                res.send(newStub);
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var missingErr;
        if (!req.body.path){
            missingErr = 'path';
        } else {
            missingErr = 'body';
        }
        var err = new Error('missing '+missingErr);
        err.status = 400;
        next(err);
    }
});

function parseSQLError(err) {
    try{
        if(err && err.message){
            if(err.message.includes('SQLITE_CONSTRAINT: UNIQUE')){
                err.message = "A mapping with this path and method already exist";
            }
        }
        err.status = 400;
    } catch (error){
        err = error;
    }
    return err;
}

module.exports = router;
