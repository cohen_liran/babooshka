"use strict";

var express = require('express');
var router = express.Router();
let db = require('../database/database');

router.use('/stub',require('./stub'));
router.use('/channel',require('./channel'));
router.use('/settings',require('./settings'));

module.exports = router;
