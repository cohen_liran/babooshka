"use strict";

var express = require('express');
var router = express.Router();

router.use('/babooshka',require('./babooshka'));
router.use(require('./proxy'));

module.exports = router;
