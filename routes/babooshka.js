"use strict";

var express = require('express');
var router = express.Router();
var path = require('path');
var isAllowedBrowser = require("../scripts/utils").isAllowedBrowser;
var servePage = require('../scripts/utils').servePage;

router.use(require('../middlewares/jsonParser'));
router.use('/api',require('./api'));

router.get('/*', function(req,res,next){
    if(!isAllowedBrowser(req)) {
        servePage('browser-not-supported.html',res,next);
    } else {
        next();
    }
});

router.use(express.static(path.join(__dirname, '../public')));

router.get('/*', function(req,res,next){
    servePage('index.html',res,next);
});

//error handler
router.use(function(err, req, res, next) {
    var statusCode = err.status || 500;
    res.status(statusCode);
    if(statusCode === 500){
        console.error(err);
    }
    res.send({
        message: err.message,
        error: err
    });
});

module.exports = router;