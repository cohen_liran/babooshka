/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";
var path = require('path');
var fs = require('fs');
var parser = require('ua-parser-js');

const SUPPORTED_BROWSERS = [
    "firefox",
    "chrome",
];

const PUBLIC_DIR = path.resolve('./public');

module.exports.getEnv = function(paramName, def){
    return process.env["npm_package_config_"+paramName] || process.env[paramName] || def;
};

module.exports.readStaticFile = function(file){
    return new Promise(function(onResult, onError){
        var filePath = path.join(PUBLIC_DIR, file);
        fs.readFile(filePath, function(err, data) {
            if (err) {
                onError(err);
            } else {
                onResult(data);
            }
        });
    });
};

module.exports.servePage = function(file,res,next){
    module.exports.readStaticFile(file)
        .then(data=>{
            res.write(data);
            res.end();
        })
        .catch(err=>{
            //try to load 404 page
            return module.exports.readStaticFile('404.html'); 
        })
        .then(pageNotFoundPage=>{
            //if pageNotFoundPage is undefined this means our flow has resulted in a success
            if(pageNotFoundPage){
                //successfully loaded the 404 page, send it back
                res.status(404);
                res.write(pageNotFoundPage);
                return pageNotFoundPage.end();
            }
        })
        .catch(err=>{
            //failed to load 404 page, let other error handlers handle this
            var error = new Error('Not Found');
            error.status = 404;
            next(error);
        });
};

module.exports.isAllowedBrowser = function(req){
    try{
        let userAgentParser = parser(req.headers['user-agent']);
        let browserName = userAgentParser.browser.name.toLowerCase();

        for(let browser of SUPPORTED_BROWSERS){
            if(browser === browserName) return true;
        }
    } catch (e){
        console.error(e);
    }
    return false;
};