"use strict";

const BODY_ERR = 'BodyParseException, ';

var parseJsonBody = function(req, res, next) {
    var body = [];
    if(req.headers['content-length'] > 0){
        req.on('data', function (chunk) {
            body.push(chunk);
        }).on('error', function(e){
            e.message = BODY_ERR.concat(e.message);
        }).on('end', function() {
            req.body = Buffer.concat(body).toString() || "";
            next();
        });
    } else {
        next();
    }
};

module.exports =  parseJsonBody;
