"use strict";

var checkHasBody = function(req, res, next) {
    req.hasBody = !isNullOrEmpty(req.body);
    req.hasQuery = !isNullOrEmpty(req.query);
    next();
};

var isNullOrEmpty = function(obj){
    return !obj || (Object.keys(obj).length === 0 && obj.constructor === Object);
};

module.exports =  checkHasBody;
