/**
 * Created by Liran on 15-Feb-17.
 */
"use strict";

const CONFIG = {

    BASE_URL : '',
    BASE_URI : 'babooshka',
    API_URI : 'api',
    ERROR_DURATION_MS : 5000,
    MESSAGE_DURATION_MS : 5000,
    
    setBaseUrl : function(url){
        CONFIG.BASE_URL = url.slice(0, url.indexOf('/', 8));
        CONFIG.BASE_URI = url.slice(CONFIG.BASE_URL.length + 1, url.lastIndexOf('babooshka') + 'babooshka'.length);
        if (CONFIG.BASE_URI.endsWith('/')) {
            CONFIG.BASE_URI = CONFIG.BASE_URI.slice(0, CONFIG.BASE_URI.length - 1);
        }
    },
    
    getBaseUrl : function(){
        return CONFIG.BASE_URL+'/'+CONFIG.BASE_URI;
    },
    
    getApiUrl : function(){
        return CONFIG.getBaseUrl()+'/'+CONFIG.API_URI;
    },
    
    getUrlFromUri : function(uri){
        return CONFIG.getBaseUrl() +'/'+ uri;
    }
};