/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";
(function(){
    
    var module = angular.module('settings',[
        
    ]);
    module.controller('SettingsController', function ($scope, $timeout, $http){

        this.getUrl = CONFIG.getUrlFromUri;

        this.getSettings = function(){
            getSettings($scope, $http, $timeout)
                .then(settings=>{
                    this.restoreSettings();
                }).catch(err=>{
                console.log(err);
            });
        };

        this.getSettings();

        this.updateSettings = function(){
            updateSettings($scope, $http)
                .catch(err=>{
                    console.log(JSON.stringify(err));
                    showGlobalMessage($timeout, $scope, {message: err.data.message, type:'error'});
                    throw err;
                })
                .then(()=>{
                    //assign data as the new data
                    $scope.forwardUrl = $scope.temp.forwardUrl;
                    $scope.forwardProtocol = $scope.temp.forwardProtocol;
                    $scope.validateJSONResponse = $scope.temp.validateJSONResponse;
                    $scope.allowSelfSignedCert = $scope.temp.allowSelfSignedCert;
                    $scope.allowChannelFallThrough = $scope.temp.allowChannelFallThrough;
                    this.restoreSettings();
                    showGlobalMessage($timeout, $scope, {message: "Settings updated successfully", type:'success'});
                }).catch(err=>{
                console.log(err);
            });
        };

        this.restoreSettings = function(){
            if(!$scope.temp) $scope.temp = {};
            $scope.temp.forwardUrl = $scope.forwardUrl;
            $scope.temp.forwardProtocol = $scope.forwardProtocol;
            $scope.temp.validateJSONResponse = $scope.validateJSONResponse;
            $scope.temp.allowSelfSignedCert = $scope.allowSelfSignedCert;
            $scope.temp.allowChannelFallThrough = $scope.allowChannelFallThrough;
        };

        $scope.stringify = JSON.stringify;
    });

    let getSettings = function($scope, $http, $timeout){
        return $http.get(CONFIG.getApiUrl()+'/settings',
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                showGlobalMessage($timeout, $scope, {message: err.data.message, type:'error'});
            })
            .then(settings=>{
                $scope.forwardUrl = settings.data.forwardUrl;
                $scope.forwardProtocol = settings.data.forwardProtocol;
                $scope.validateJSONResponse = Boolean(Number(settings.data.validateJSONResponse));
                $scope.allowSelfSignedCert = Boolean(Number(settings.data.allowSelfSignedCert));
                $scope.allowChannelFallThrough = Boolean(Number(settings.data.allowChannelFallThrough));
            });
    };

    let updateSettings = function($scope, $http){
        let settings = {
            forwardUrl:$scope.temp.forwardUrl,
            forwardProtocol:$scope.temp.forwardProtocol,
            validateJSONResponse:$scope.temp.validateJSONResponse,
            allowSelfSignedCert:$scope.temp.allowSelfSignedCert,
            allowChannelFallThrough:$scope.temp.allowChannelFallThrough
        };
        return $http.put(CONFIG.getApiUrl()+'/settings', settings,
            {responseType:'json'})
    };

    let showGlobalMessage = function($timeout, $scope, message, keepOn){
        if(!$scope.messages){
            $scope.messages = [];
        }
        if(!keepOn){
            message.promise = $timeout(function(){

                var index = $scope.messages.indexOf(message);
                $scope.messages.splice(index, 1);

            }, CONFIG.MESSAGE_DURATION_MS);
        }
        $scope.messages.push(message);
    };
})();