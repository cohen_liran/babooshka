/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";

(function(){

    var DEFAULT_CHANNEL = {_id: -1, channel:'default', indexing: false};
    
    var module = angular.module('stubs',[
        'ngSanitize',
        'toggle-switch'
    ]);
    
    module.controller('StubsController', function ($timeout, $scope, $http){

        this.getUrl = CONFIG.getUrlFromUri;

        $scope.clearNewStub = function(closeContainer){
            if(!$scope.newStub){
                $scope.newStub = {};
            }
            $scope.newStub.code = 200;
            $scope.newStub.method = 'GET';
            $scope.newStub.active = 1;
            $scope.newStub.response = undefined;
            $scope.newStub.alias = undefined;
            $scope.newStub.path = undefined;
            $scope.newStub.messages = undefined;
            $scope.newStub.channel = undefined;
            $scope.newStub.delay = 0;
            $scope.newStub.temp = {code: 200, active: 1, method: 'GET', delay: 0};
            if(closeContainer){
                $scope.newStub.opened = undefined;
            }
        };

        $scope.clearNewStub();
        $scope.channels = [DEFAULT_CHANNEL];
        $scope.selectedChannel = DEFAULT_CHANNEL;
        $scope.stubsAll = [];
        $scope.stubs = [];

        getSettings($scope, $http);
        getChannels($scope, $http);
        getStubs($scope, $http);

        this.refreshWarnings = function(stub){
            stub.temp.responseDecorated = undefined;
            delete stub.temp.allowedBadResponse;
            if(stub.messages){
                stub.messages = stub.messages.filter(function(msg){
                    return msg.type !== 'lint-warn'
                });
            }
        };

        this.toggleActive = function(stub){
            if(stub.metadata && stub.metadata.ignoreToggle){
                stub.metadata.ignoreToggle = undefined;
                return;
            }
            stub.temp.active = stub.active ? 0 : 1;
            updateStubActiveState($scope, $http, stub.temp)
                .catch(err=>{
                    console.log(JSON.stringify(err));
                    showStubMessage($timeout, stub, {message: err.data.message, type:'error'});
                    //restore old data to the stub
                    if(!stub.metadata) stub.metadata = {};
                    stub.metadata.ignoreToggle = undefined;
                    stub.temp.active = stub.active;
                })
                .then(function(){
                    //assign data as the new data
                    stub.active = stub.temp.active;
                });
        };

        this.addStub = function(event){
            if(validateResponse($scope, $timeout,$scope.newStub,
                    $(event.target).parent().parent().find('#response').get(0)) || $scope.newStub.temp.allowedBadResponse){
                delete $scope.newStub.temp.allowedBadResponse;
                $scope.newStub.temp.channel=$scope.selectedChannel._id;
                addStub($scope, $http, $timeout, $scope.newStub.temp);
            } else if($scope.newStub.temp.allowedBadResponse !== undefined){
                $scope.newStub.temp.allowedBadResponse = window.confirm('Are you sure you wish to save?\nThe response body is in invalid json format');
                if($scope.newStub.temp.allowedBadResponse){
                    this.addStub(event);
                }
            } else {
                $scope.newStub.temp.allowedBadResponse = false;
            }
        };
        
        this.updateStub = function(stub, event){
            if(validateResponse($scope, $timeout,stub,
                    $(event.target).parent().parent().find('#response').get(0)) || stub.temp.allowedBadResponse){
                updateStub($scope, $http, stub.temp)
                    .catch(err=>{
                        console.log(JSON.stringify(err));
                        showStubMessage($timeout, stub, {message: err.data.message, type:'error'});
                    })
                    .then(function(){
                        //assign data as the new data
                        stub.temp.channel=$scope.selectedChannel._id;
                        delete stub.temp.allowedBadResponse;
                        stub.alias = stub.temp.alias;
                        stub.path = stub.temp.path;
                        stub.response = stub.temp.response;
                        stub.code = stub.temp.code;
                        stub.method = stub.temp.method;
                        stub.channel = stub.temp.channel;
                        stub.delay = stub.temp.delay;
                        showStubMessage($timeout, stub, {message: "Mapping updated successfully", type:'success'});
                    });
            } else if(stub.temp.allowedBadResponse !== undefined){
                stub.temp.allowedBadResponse = window.confirm('Are you sure you wish to save?\nThe response body is in invalid json format');
                if(stub.temp.allowedBadResponse){
                    this.updateStub(stub,event);
                }
            } else {
                stub.temp.allowedBadResponse = false;
            }
        };

        this.deleteStub = function(stub){
            var confirm = window.confirm('Are you sure you wish to delete this mapping?\nThe operation cannot be undone');
            if(confirm){
                deleteStub($scope, $http, stub._id)
                    .catch(err=>{
                        showStubMessage($timeout, stub, {message: err.data.message, type:'error'});
                    })
                    .then(function() {
                        showGlobalMessage($timeout, $scope,
                            {
                                message:'Mapping removed successfully',
                                type:'success'
                            });
                    });
            }
        };

        this.toggleShow = function(stub){
            this.i ++;
            stub.opened = !stub.opened;
        };

        this.getHighlightedHeader = function(path) {
            return path.replace(/<\?>/gi, '<span class="wild-card-mark">&lt;?&gt;</span>');
        };

        this.selectChannel = function (channel) {
            selectChannel($scope, channel);
        };

        this.toggleIndexing = function(){
            if($scope.selectedChannel.indexing) {
                if (confirm("Are you sure you wish to stop indexing the current channel ?")) {
                    setIndexing($scope, $http, $timeout, $scope.selectedChannel._id, false);
                }
            } else {
                if (confirm("Live indexing records traffic going through Babooshka to the remote server and construct (or overwrite) stubs based on successful responses retrieved.\nDuring live indexing Babooshka will always forward requests to the remote server when accessing the current channel until you actively stop indexing.\n\nWould you like to start live indexing now?")) {
                    setIndexing($scope, $http, $timeout, $scope.selectedChannel._id, true);
                }
            }
        };

        $scope.stringify = JSON.stringify;
    });

    let getSettings = function($scope, $http){
        return $http.get(CONFIG.getApiUrl()+'/settings',
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
            })
            .then(settings=>{
                $scope.forwardUrl = settings.data.forwardUrl;
                $scope.validateJSONResponse = Boolean(Number(settings.data.validateJSONResponse));
            });
    };

    let getChannels = function($scope, $http){
        return $http.get(CONFIG.getApiUrl()+'/channel',
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                $scope.channels = [DEFAULT_CHANNEL];
                selectChannel($scope, DEFAULT_CHANNEL);
            })
            .then(channels=>{
                for(var channel of channels.data){
                    channel.temp = clone(channel);
                }
                $scope.channels = [DEFAULT_CHANNEL].concat(channels.data);
                selectChannel($scope, DEFAULT_CHANNEL)
            }).catch(e=>{
                console.log(e);
            });
    };

    let getStubs = function($scope, $http){
        return $http.get(CONFIG.getApiUrl()+'/stub',
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                $scope.stubsAll = [];
                $scope.stubs = [];
            })
            .then(stubs=>{
                for(var stub of stubs.data){
                    stub.temp = clone(stub);
                }
                $scope.stubsAll = stubs.data;
                $scope.stubs = [];
                selectChannel($scope, $scope.selectedChannel);
                setTimeout(bindScrollers, 250);
            }).catch(e=>{
                console.log(e);
            });
    };

    let selectChannel = function($scope, channel){
        $scope.selectedChannel=channel;
        $scope.stubs = $scope.stubsAll.filter((stub)=>{
            return stub.channel === channel._id
        });
    };

    let bindScrollers = function(){
        let containers = $(".editField",".stub-item");

        let containerIndex = 0;
        while(containers.hasOwnProperty(containerIndex)){
            let container = $(containers[''+containerIndex]);
            let slave = $(container.children()['0']);
            let master = $(container.children()['1']);

            let sync = function(){
                let $other = slave.off('scroll'), other = $other.get(0);
                //let percentage = this.scrollTop / (this.scrollHeight - this.offsetHeight);
                other.scrollTop = Math.min(other.scrollHeight, this.scrollTop);//percentage * (other.scrollHeight - other.offsetHeight);
                setTimeout( function(){ $other.on('scroll', sync ); },10);
            };
            master.on('scroll', sync);
            containerIndex++;
        }
    };

    let updateStub = function($scope, $http, stub){
        return $http.put(CONFIG.getApiUrl()+'/stub', stub,
            {responseType:'json'})
    };

    let updateStubActiveState = function($scope, $http, stub){
        return $http.put(CONFIG.getApiUrl()+'/stub/toggle', stub,
            {responseType:'json'})
    };

    let deleteStub = function($scope, $http, id){
        return $http.delete(CONFIG.getApiUrl()+'/stub?id='+id,
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
            })
            .then(function(){
                $scope.stubs = $scope.stubs.filter(function(stub) {
                    return stub._id !== id;
                });
            });
    };

    let addStub = function($scope, $http, $timeout, stub){
        return $http.post(CONFIG.getApiUrl()+'/stub',stub,
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                showGlobalMessage($timeout, $scope, {message: err.data.message, type:'error'});
            })
            .then(newStub=>{
                if(newStub){
                    $scope.clearNewStub(true);
                    showGlobalMessage($timeout, $scope,
                        {
                            message:'Mapping added successfully',
                            type:'success'
                        });
                    return getStubs($scope, $http);
                }
            });
    };

    let showStubMessage = function($timeout, stub, message, keepOn){
        if(!stub.messages){
            stub.messages = [];
        }

        if(!keepOn){
            message.promise = $timeout(function(){
                var index = stub.messages.indexOf(message);
                stub.messages.splice(index, 1);

            }, CONFIG.ERROR_DURATION_MS);
        }
        stub.messages.push(message);
    };

    let showGlobalMessage = function($timeout, $scope, message, keepOn){
        if(!$scope.messages){
            $scope.messages = [];
        }
        if(!keepOn){
            message.promise = $timeout(function(){

                var index = $scope.messages.indexOf(message);
                $scope.messages.splice(index, 1);

            }, CONFIG.MESSAGE_DURATION_MS);
        }
        $scope.messages.push(message);
    };

    let setIndexing = function($scope, $http, $timeout, channelId, shouldIndex){
        return $http.post(CONFIG.getApiUrl()+'/channel/live-indexing',
            {channel:channelId, index:shouldIndex},
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                showGlobalMessage($timeout, $scope, {message: err.data.message, type:'error'});
            })
            .then(updatedChannel=>{
                console.log(updatedChannel.data);
                if(updatedChannel && updatedChannel.data){
                    var channel = $scope.channels.find(function(ch){
                        return ch._id === updatedChannel.data._id;
                    });
                    channel.indexing = updatedChannel.data.indexing;
                    if(!updatedChannel.data.indexing){
                        //reload stubs
                        return getStubs($scope, $http);
                    }
                }
            });
    };

    let validateResponse = function($scope, $timeout, stub, textarea){
        if(stub.messages){
            stub.messages = stub.messages.filter(function(msg){
                return msg.type !== 'lint-warn'
            });
        }

        if($scope.validateJSONResponse && stub.temp.response){
            try{
                var parsed = JSON.parse(stub.temp.response);
                stub.temp.response = JSON.stringify(parsed, null , 3);
            } catch(e){
                try{
                    //WINDOWS
                    var regex;
                    var lines = stub.temp.response ? stub.temp.response.split(/\n/) : 0;
                    var errorLineNumber = -1;
                    try{
                        regex = /line (\d+) column \d+/g;
                        errorLineNumber = regex.exec(e.message)[1] -1;

                        stub.temp.responseDecorated = "";

                        for(let i=0; i<lines.length; i++){
                            if(i === errorLineNumber){
                                stub.temp.responseDecorated += '<span class="syntax-error">'+lines[i]+'</span>';
                            } else {
                                stub.temp.responseDecorated += lines[i];
                            }
                            if(i < lines.length - 1){
                                stub.temp.responseDecorated += '<br/>';
                            }
                        }
                    } catch (notWindowsExcept){
                        //MAC OS
                        regex = /position (\d+)/g;
                        var errorCharNumber = Number(regex.exec(e.message)[1]);

                        stub.temp.responseDecorated = "";

                        errorLineNumber = -1;
                        var totalCharacters = 0;
                        for(let i=0; i<lines.length; i++){
                            if(lines[i].length + totalCharacters >= errorCharNumber && errorLineNumber < 0){
                                errorLineNumber = i;
                                stub.temp.responseDecorated += '<span class="syntax-error">'+lines[i]+'</span>';
                            } else {
                                stub.temp.responseDecorated += lines[i];
                            }
                            if(i < lines.length - 1){
                                stub.temp.responseDecorated += '<br/>'
                            }
                            totalCharacters += lines[i].length;
                        }
                    }

                    textarea.scrollTop = Math.max(0, textarea.scrollHeight * (errorLineNumber / lines.length) - 40);

                } catch (unknownErrorSyntaxExcept){
                    console.log(unknownErrorSyntaxExcept);
                }

                showStubMessage($timeout, stub, {message: parseLintError(e), type:'lint-warn'}, true);
                return false;
            }
        }
        return true;
    };

    let parseLintError = function(error){
        return error.message.replace('JSON.parse','JSON Error');
    };

})();