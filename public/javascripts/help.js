/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";

(function(){

    var module = angular.module('help',[
    ]);

    module.controller('HelpController', function (){
        this.getUrl = CONFIG.getUrlFromUri;
    });

})();