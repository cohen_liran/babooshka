/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";

(function(){

    var module = angular.module('channels',[
        'ngSanitize'
    ]);

    module.controller('ChannelsController', function ($timeout, $scope, $http){

        this.getUrl = CONFIG.getUrlFromUri;

        $scope.clearNewChannel = function(closeContainer){
            if(!$scope.newChannel){
                $scope.newChannel = {};
            }
            $scope.newChannel.channel = undefined;
            $scope.newChannel.temp = {};
            if(closeContainer){
                $scope.newChannel.opened = undefined;
            }
        };

        $scope.clearNewChannel();
        $scope.channels = [];

        getChannels($scope, $http);

        this.refreshWarnings = function(channel){
            channel.temp.responseDecorated = undefined;
            delete channel.temp.allowedBadResponse;
            if(channel.messages){
                channel.messages = channel.messages.filter(function(msg){
                    return msg.type !== 'lint-warn'
                });
            }
        };

        this.addChannel = function(){
            var err = verifyChannel($scope.newChannel.temp.channel);
            if(err){
                showChannelMessage($timeout, $scope.newChannel, {message: err.data.message, type: 'error'});
            } else {
                addChannel($scope, $http, $timeout, $scope.newChannel.temp);
            }
        };

        this.updateChannel = function(channel, event){
            var err = verifyChannel(channel.temp.channel);
            if(err){
                showChannelMessage($timeout, channel, {message: err.data.message, type: 'error'});
            } else {
                updateChannel($scope, $http, channel.temp)
                    .then(function () {
                        //assign data as the new data
                        channel.channel = channel.temp.channel;
                        showChannelMessage($timeout, channel, {
                            message: "Channel updated successfully",
                            type: 'success'
                        });
                    })
                    .catch(err => {
                        console.log(JSON.stringify(err));
                        showChannelMessage($timeout, channel, {message: err.data.message, type: 'error'});
                    });
            }
        };

        this.deleteChannel = function(channel){
            var confirm = window.confirm('Are you sure you wish to delete this channel?\nAssociated mappings will be deleted as well\nThe operation cannot be undone');
            if(confirm){
                deleteChannel($scope, $http, channel._id)
                    .catch(err=>{
                        showChannelMessage($timeout, channel, {message: err.data.message, type:'error'});
                    })
                    .then(function() {
                        showGlobalMessage($timeout, $scope,
                            {
                                message:'Channel removed successfully',
                                type:'success'
                            });
                    });
            }
        };

        this.toggleShow = function(channel){
            channel.opened = !channel.opened;
        };

        $scope.stringify = JSON.stringify;
    });

    let verifyChannel = function(channel){
        if(!channel){
            return {
                data: {
                    message: 'Channel cannot be left empty'
                }
            }
        }
        else if(new RegExp(/[^A-z\d-]/gi).test(channel)){
            return {
                data: {
                    message: 'Channel may only contain letters, numbers and - sign'
                }
            }
        }
    };

    let getChannels = function($scope, $http){
        return $http.get(CONFIG.getApiUrl()+'/channel',
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                $scope.channels = [];
            })
            .then(channels=>{
                for(var channel of channels.data){
                    channel.temp = clone(channel);
                }
                $scope.channels = channels.data;
            }).catch(e=>{
                console.log(e);
            });
    };

    let updateChannel = function($scope, $http, channel){
        return $http.put(CONFIG.getApiUrl()+'/channel', channel,
            {responseType:'json'})
    };

    let deleteChannel = function($scope, $http, id){
        return $http.delete(CONFIG.getApiUrl()+'/channel?id='+id,
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
            })
            .then(function(){
                $scope.channels = $scope.channels.filter(function(channel) {
                    return channel._id !== id;
                });
            });
    };

    let addChannel = function($scope, $http, $timeout, channel){
        return $http.post(CONFIG.getApiUrl()+'/channel',channel,
            {responseType:'json'})
            .catch(err=>{
                console.log(JSON.stringify(err));
                showGlobalMessage($timeout, $scope, {message: err.data.message, type:'error'});
            })
            .then(newChannel=>{
                if(newChannel){
                    $scope.clearNewChannel(true);
                    showGlobalMessage($timeout, $scope,
                        {
                            message:'Channel added successfully',
                            type:'success'
                        });
                    return getChannels($scope, $http);
                }
            });
    };

    let showChannelMessage = function($timeout, channel, message, keepOn){
        if(!channel.messages){
            channel.messages = [];
        }

        if(!keepOn){
            message.promise = $timeout(function(){
                var index = channel.messages.indexOf(message);
                channel.messages.splice(index, 1);

            }, CONFIG.ERROR_DURATION_MS);
        }
        channel.messages.push(message);
    };

    let showGlobalMessage = function($timeout, $scope, message, keepOn){
        if(!$scope.messages){
            $scope.messages = [];
        }
        if(!keepOn){
            message.promise = $timeout(function(){

                var index = $scope.messages.indexOf(message);
                $scope.messages.splice(index, 1);

            }, CONFIG.MESSAGE_DURATION_MS);
        }
        $scope.messages.push(message);
    };

})();