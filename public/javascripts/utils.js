/**
 * Created by Liran on 16-Feb-17.
 */
"use strict";

let updateObject = function(target, update){
    for(let key of Object.keys(target)){
        if(update.hasOwnProperty(key)){
            target[key] = update[key]
        }
    }
};

let clone = function(obj){
    return  JSON.parse(JSON.stringify(obj));
};