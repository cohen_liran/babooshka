/**
 * Created by Liran on 14-Feb-17.
 */
"use strict";

(function(){

    const PAGE_NOT_FOUND_ROUTE = { name: '404', url: 'notfound' , templateUrl: 'page-not-found.html'};

    const ROUTES = [
        { name: 'Home', url: '' , templateUrl: 'stubs.html'},
        { name: 'Channels', url: 'channels' , templateUrl: 'channels.html'},
        { name: 'Settings', url: 'settings' , templateUrl: 'settings.html'},
        { name: 'Help', url: 'help' , templateUrl: 'help.html'},
        { name: 'About', url: 'about' , templateUrl: 'about.html'}
    ];

    const removeUrlPrefix = function(url){
        let currentPath = url;

        if(currentPath.startsWith(CONFIG.BASE_URL)){
            currentPath = currentPath.slice(CONFIG.BASE_URL.length);
        }

        while(currentPath.startsWith('/')){
            currentPath = currentPath.slice(1);
        }
        if(currentPath.startsWith(CONFIG.BASE_URI)){
            currentPath = currentPath.slice(CONFIG.BASE_URI.length+1);
        }
        return currentPath;
    };

    var getRouteUri = function(route){
        return '/'+CONFIG.BASE_URI +'/'+ route.url;
    };

    var module = angular.module('app',[
        'ngRoute',
        'ngAnimate',
        'stubs',
        'settings',
        'channels',
        'help',
        'about',
    ]);

    module.config(function($routeProvider, $locationProvider){
        CONFIG.setBaseUrl(window.location.href);
        var registerRoute = function(route){
            $routeProvider
                .when(getRouteUri(route),{
                    templateUrl: CONFIG.BASE_URI+'/pages/'+route.templateUrl,
                    controller: function($scope){
                        $scope.pageClass=route.name;
                    }
                });
        };

        //register all routes
        for(let route of ROUTES){
            registerRoute(route);
        }

        //set fallback route
        registerRoute(PAGE_NOT_FOUND_ROUTE);
        $routeProvider
            .otherwise({redirectTo: getRouteUri(PAGE_NOT_FOUND_ROUTE)});

        //make pretty links without hashtag
        $locationProvider.html5Mode(true);
    });

    module.controller('AppController', function ($location){
    });
    
    module.controller('MenuController', function ($location){
        this.routes = ROUTES;

        this.isActivePage = function(index){
            let currentPath = removeUrlPrefix($location.absUrl());
            let checkedPath = removeUrlPrefix(this.routes[index].url);
            if(currentPath.length === 0 || checkedPath.length === 0){
                return currentPath === checkedPath;
            } else {
                return currentPath.startsWith(checkedPath);
            }
        };
        
        this.getUrl = CONFIG.getUrlFromUri;
    });

    module.directive('navBar',function($location){
        return {
            restrict: 'E',
            templateUrl: CONFIG.BASE_URI+'/html-components/menu.html',
            controller: 'MenuController',
            controllerAs: 'menu'
        };
    });
})();