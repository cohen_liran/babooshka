/**
 * Created by Liran on 28-Mar-17.
 */
"use strict";
(function(){

    var module = angular.module('about',[

    ]);
    module.controller('AboutController', function ($scope, $timeout, $http){

        $scope.version = 'loading';

        this.getSettings = function(){
            getSettings($scope, $http, $timeout)
                .catch(err=>{
                console.log(err);
            });
        };

        this.getSettings();
    });

    let getSettings = function($scope, $http){
        return $http.get(CONFIG.getApiUrl()+'/settings',
            {responseType:'json'})
            .catch(err=>{
                console.log(err);
            })
            .then(settings=>{
                console.log(settings.data);
                $scope.version = settings.data.version;
            });
    };
})();