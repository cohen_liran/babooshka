# Babooshka #

Server side and client side developers often run across each-others path when adding new functionality to an already existing system. Whether the client developer has to wait for the server side to be developed or the server's developers have to waste time altering their code to return stub data while working, being able to work together without stepping on each others toes is a challenge for even an experienced team, and that's where Babooshka comes in. 

### What is this repository for? ###

Baboshka is a stub serving forward proxy server aimed to speed up development process by letting both
server and client developers work simultaneously without obstructing each other.
    
Babooshka sits between your development server and your client and either forward the requests made to the
server, or return a predefined stub response.
This prevent an unfinished server side to hinder client
development and remove pressure from server developers. Babooshka is easily configurable and is targeted
towards client developers, avoiding complex configurations and messy controls. Your client guys could literally
control it on their own and disconnect their stubs when server is ready. Hardly no code modification required
on both client and server side.

![diagram.png](https://bitbucket.org/repo/75yke9/images/3469421498-diagram.png)

### Features ###

* Easy to set up.
* No code modification required on either the server side or client's side
* Simple GUI

![Untitled-0.png](https://bitbucket.org/repo/75yke9/images/1989626757-Untitled-0.png)

### How do I get set up? ###

* Clone/download this repository
* Install [Node.js (version 5+)](https://nodejs.org/en/)
* Open terminal/command line
* Navigate to Babooshka's directory (the one containing package.json)
* Install Babooshka's dependencies by calling:

```
#!bat

npm install -r package.json
```
* Run Babooshka by calling:

```
#!bat

npm run prod
```

### Upgrading from older versions ###

Unfortunately version 3 is not compatible with older databases. you will have to replace your old
installation with a fresh copy to migrate to version 3

### Need more help? ###

* Open your browser and enter localhost:3000/babooshka
* Click on help to read the included help section for usage details